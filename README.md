## Contact management system
This is the codebase for a web application hosted [here](https://contactsmanagement.herokuapp.com). It allows registration of users to a platform.

This is a Python Django app running on `Django 2`. All Python package dependencies can be gleaned from the `Pipfile`.

### Deliverables
1. Link to live project: https://contactsmanagement.herokuapp.com
2. Repository: https://gitlab.com/erickmbwana/contactsmanagement.git

### Scope:
The application provides the following functionality:
- Front end interface to register, update, delete and list users
- Validation of data before saving it in the database
- Rest endpoints to perform the above listed operations
- Searching registered users by name, phone number or email address.
- Pagination of results in both the REST API and frontend
- Management command to register users from a csv file
- Management command to remove registered based on date ranges.
- Downloading of records as Excel spreadsheets
- A graph showing a breakdown of all users by age.

### Assumptions
In the development of this app some key assumptions were made:
- No authentication is needed for both the front end and the REST endpoints
- In data validation, all data(name, email, phone, age) was considered mandatory.
- User data was considered duplicate if it had all the four fields as duplicates(repeated rows).

### Local Installation
The project requires Python 3.5  or newer. Package and virtual environment management uses the excellent [pipenv](https://docs.pipenv.org/) package.

Clone this repo:
`git clone https://gitlab.com/erickmbwana/contactsmanagement.git`

Within the project folder span up a new virtual environment using Python three:

`pipenv shell --three`

Install all requirements as specified in the Pipfile:

`pipenv install`

The project uses an environment variable in determining whether to  use production or  development settings,
defaulting to production settings if the environment variable is not found.
To use the development settings add the following environment variable:

`DJANGO_MODE=DEVELOPMENT`

After adding the environment variable, run migrations to persist changes in the database:

`python manage.py migrate`

By default the development configuration uses the Sqlite database for local development. The production site uses PostgreSQL.
To change from Sqlite to PostgreSQL in your local machine add a `local.py` file in the `settings/` folder and add the database config as it suits you local set up:

```python
# contactsdb/settings/local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'contactsdb',
        'USER': 'XXXXX',
        'PORT': '5432',
        'HOST': 'localhost',
        'PASSWORD': 'XXXX'
    }
}

```

You can then run the project: `python manage.py runserver`

### Tests
The project uses pytest for testing. All tests are located `main/tests`

To run the tests: `pytest`

There is a csv file in the project and a json fixture for testing purposes.
You can load the fixture thus:

`python manage.py loaddata fixtures/contacts.json`

Or you can also upload the csv using the front end or the management command.

### Management commands
The app has two management commands:
1. `python manage.py deleterecordss --from-date YYYY/MM/DD --to-date YYYY/MM/DD`

    The commands accepts two optional arguments: `--from-date` and `--to-date`.
    - If both are provided contacts registered within that range will be deleted.
    - If only `--from-date` is provided, users registered from that date to today will be deleted.
    - If only `--to-date` is provided, all users registered from that date backwards will be deleted.
    - If none of them is provided, all registered users will be deleted.

2. `python manage.py importrecords <path to csv file>`

    This command takes one mandatory argument: path to the CSV file with users to register.
    If validation is successful, the records will create(registered) in the database.

    For example using the CSV file in the project:

    `python manage.py importrecords contactsdb.csv`

### Asset management
The project uses `npm` for front end asset management.
The front end dependencies are listed in `package.json`

You can upgrade/downgrade any of the front end dependencies using `npm` and thereafter copy the updated/downgraded
files to the project's static file directories using this command:

`npm run assets`

The project uses the awesome `Semantic UI` CSS framework for styling.

### REST API
No authentication is needed to access the API.

It provides the following resources:

1. `User(Contact)`

The application exposes the following REST endpoints(no authentication is required) for the user resource:

|                     | GET                     | POST                | PUT                       | DELETE                    |
|---------------------|-------------------------|---------------------|---------------------------|---------------------------|
| api/v1/users        | Show all users          | Add a new  category | N/A                       | Delete all users          |
| api/v1/users/`id`   | show user with `id`     | N/A                 | Update user with `id`     | Delete user with `id`     |


The full documentation of the REST API is here: https://contactsmanagement.herokuapp.com/api/v1/docs/

Or locally at: `/api/v1/docs`
