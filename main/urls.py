from django.urls import include, path

from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from . import views
from .viewsets import ContactViewSet


router = routers.DefaultRouter()
router.register(r'users', ContactViewSet)


urlpatterns = [
    path('', views.ContactListView.as_view(), name='contacts'),
    path('user', views.ContactCreateView.as_view(), name='contact'),
    path('upload', views.ContactImportFormView.as_view(), name='contacts_upload'),
    path('user/<int:pk>/', views.ContactUpdateView.as_view(), name='contact_update'),
    path('user/<int:pk>/delete/', views.ContactDeleteView.as_view(), name='contact_delete'),
    path('users/download', views.ContactDownloadView.as_view(), name='contacts_download'),

    # DRF
    path('api/v1/', include(router.urls)),
    path('api/v1/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/docs/', include_docs_urls(title='Contacts Management System API', public=False))
]
