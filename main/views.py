from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.core.files.base import ContentFile
from django.http import FileResponse
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView, DeleteView, FormView, ListView, UpdateView, View
)
from django.utils import timezone

from tablib import Dataset

from .forms import ContactForm, ContactImportForm, ContactSearchForm
from .models import Contact
from .utils import GRAPH_BACKGROUND_COLORS, GRAPH_BORDER_COLORS


class ContactCreateView(SuccessMessageMixin, CreateView):

    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contacts')
    success_message = 'The new contact has been added successfully'


class ContactUpdateView(SuccessMessageMixin, UpdateView):

    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('contacts')

    def get_success_message(self, cleaned_data):
        name = cleaned_data['name']
        return "{} contact details have been updated successfully.".format(name)


class ContactDeleteView(SuccessMessageMixin, DeleteView):

    model = Contact
    success_url = reverse_lazy('contacts')
    success_message = 'The user been deleted successfully.'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


class ContactListView(ListView):

    model = Contact
    context_object_name = 'contacts'
    paginate_by = 70
    paginated_orphans = 5

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['search_form'] = ContactSearchForm
        queryset = Contact.objects.all()
        counts = [
            queryset.filter(age__lte=25).count(),
            queryset.filter(age__in=range(26, 36)).count(),
            queryset.filter(age__in=range(36, 51)).count(),
            queryset.filter(age__gt=50).count()
        ]
        data = {
            'labels': ["Age 0-25", "Age 25-35", "Age 35-50", "Above 50"],
            'datasets': [{
                'label': 'Registered users by age',
                'data': counts,
                'borderWidth': 1,
                'backgroundColor': GRAPH_BACKGROUND_COLORS,
                'borderColor': GRAPH_BORDER_COLORS
            }]
        }
        ctx['graph_data'] = data
        return ctx

    def get_queryset(self):
        queryset = Contact.objects.all()
        form = ContactSearchForm(data=self.request.GET)
        if form.is_valid():
            filters = form.get_filters(form.cleaned_data)
            return queryset.filter(**filters)
        return queryset


class ContactImportFormView(FormView):

    form_class = ContactImportForm
    template_name = 'main/contacts_upload.html'
    success_url = reverse_lazy('contacts')

    def form_valid(self, form):
        data = form.cleaned_data['contacts_list']
        Contact.objects.bulk_create(data)
        messages.success(self.request, "{} new users registered successfully".format(len(data)))
        return super().form_valid(form)


class ContactDownloadView(View):

    http_method_names = ['get']

    def get_dataset(self):
        headers = ['Name', 'Email', 'Phone number', 'Age', 'Date registered']
        dataset = Dataset(headers=headers)

        # Use iterator, don't load everything into memory
        for row in Contact.objects.all().iterator():
            dataset.append([row.name, row.email, row.phone_number, row.age, row.date_added])

        return dataset

    def get(self, request, *args, **kwargs):
        dataset = self.get_dataset()
        content_file = ContentFile(dataset.xlsx)

        response = FileResponse(content_file)
        response['Content-Disposition'] = 'attachment; filename="{}-registered-users.xlsx"'.format(timezone.now().date())
        return response
