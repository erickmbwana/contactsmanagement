
$(function() {
    $('#show-graph').on('click', function(){
        $('.modal').modal('show');
    })

    var ctx = $('#graph-container');
    var data = JSON.parse($("#graph-data").html());
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data
    })
})
