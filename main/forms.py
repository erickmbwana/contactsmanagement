import csv
from django import forms

from .models import Contact
from .validators import validate_file_extension, validate_csv_file_size


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = '__all__'

        help_texts = {
            'phone_number': 'Use international format, eg +2547XXXXXXX'
        }


class ContactImportForm(forms.Form):

    csv_file = forms.FileField(
        validators=[validate_file_extension, validate_csv_file_size],
        help_text='Click above to upload your file and then submit',
        widget=forms.FileInput(
            attrs={'accept': 'text/csv, .csv'}
        )
    )

    def clean_csv_file(self):
        csv_file = self.cleaned_data['csv_file']
        contacts = []
        try:
            decoded_file = csv_file.read().decode('utf-8').splitlines()
        except UnicodeDecodeError:
            raise forms.ValidationError('Sorry, the file is not a valid CSV file or is corrupted.')

        reader = csv.DictReader(decoded_file)
        valid = ['Age', 'Email', 'Name', 'Phone number']
        headers = reader.fieldnames
        if not sorted(headers) == valid:
            raise forms.ValidationError('Invalid data format in CSV file. Please check the sample.')

        for row in reader:
            data = {
                'name': row['Name'],
                'age': row['Age'],
                'phone_number': row['Phone number'],
                'email': row['Email']
            }
            contact_form = ContactForm(data=data)
            if contact_form.is_valid():
                contacts.append(Contact(**contact_form.cleaned_data))
            else:
                raise forms.ValidationError('There is invalid, missing or duplicate data in the CSV file. Please check the sample.')
        self.cleaned_data['contacts_list'] = contacts


class ContactSearchForm(forms.Form):

    name = forms.CharField(required=False, help_text='Search by name')
    email = forms.EmailField(required=False, help_text='Search by email')
    phone_number = forms.CharField(required=False, help_text='Search by phone number')

    class Meta:
        layout = [
            (
                "Three Fields",
                ("Field", "name"),
                ("Field", "phone_number"),
                ("Field", "email"),
            ),
        ]

    def get_filters(self, data):
        filters = {}
        if data.get('name'):
            filters['name__icontains'] = data['name']
        if data.get('phone_number'):
            filters['phone_number__exact'] = data['phone_number']
        if data.get('email'):
            filters['email__exact'] = data['email']
        return filters
