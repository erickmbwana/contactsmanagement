from rest_framework import viewsets
from .models import Contact

from .serializers import ContactSerializer


class ContactViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given user with id.

    list:
    Return a list of all the registered users.

    create:
    Register a new user instance.

    update:
    Update the given user with id.

    delete:
    Delete the given user with id.
    """

    serializer_class = ContactSerializer
    queryset = Contact.objects.all()
