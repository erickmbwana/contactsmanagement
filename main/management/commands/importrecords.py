import csv
from pathlib import Path

from django.core.management.base import BaseCommand, CommandError

from main.forms import ContactForm
from main.models import Contact


class Command(BaseCommand):
    '''
    User generated files are saved in the media dir. This command runs as a
    cron job every midnight clearing all files generated earlier than that day.
    '''
    help = "Register users from a supplied CSV file."

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument(
            'csv_file_path',
            help="Path to file with users data"
        )

    def handle(self, *args, **options):
        contacts = []
        path = options['csv_file_path']
        csv_file = Path(path)
        if not csv_file.is_file():
            raise CommandError('No such file: {}. Check file the path correctly.'.format(path))

        with open(path, 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            valid = ['Age', 'Email', 'Name', 'Phone number']
            headers = reader.fieldnames
            if not sorted(headers) == valid:
                raise CommandError('Invalid data format in CSV file.')

            for row in reader:
                data = {
                    'name': row['Name'],
                    'age': row['Age'],
                    'phone_number': row['Phone number'],
                    'email': row['Email']
                }
                contact_form = ContactForm(data=data)
                if contact_form.is_valid():
                    contacts.append(Contact(**contact_form.cleaned_data))
                else:
                    raise CommandError('There is invalid, missing or duplicate data in the CSV file.')

        Contact.objects.bulk_create(contacts)
        self.stdout.write(self.style.SUCCESS('{} users registered'.format(len(contacts))))
