import datetime

from django.core.management.base import BaseCommand, CommandError

from main.models import Contact


class Command(BaseCommand):

    help = "Delete all users registered within given dates or all if no dates are provided."

    def convert_to_date(self, datestring):
        if datestring:
            try:
                day = datetime.datetime.strptime(datestring, '%Y/%m/%d')
                return day.date()
            except ValueError:
                raise CommandError('Invalid date string: {}. Use format: YYYY/MM/DD'.format(datestring))

    def add_arguments(self, parser):
        parser.add_argument(
            '--from-date',
            dest='from_date',
            help='''
            Delete users registered from this day or all after this day if
            --to-date is not provided. Otherwise delete all within that date range.
            Format: YYYY/MM/DD
            ''',
        )
        parser.add_argument(
            '--to-date',
            dest='to_date',
            help='''
            Delete users registered up this date or all before this day if
            --from_date is not provided. Otherwise delete all within that date range.
            Format: YYYY/MM/DD
            ''',
        )

    def handle(self, *args, **options):
        queryset = Contact.objects.all()
        from_date = self.convert_to_date(options.get('from_date', ''))
        to_date = self.convert_to_date(options.get('to_date', ''))
        if from_date and to_date:
            queryset = queryset.filter(date_added__range=(from_date, to_date))
        elif from_date:
            queryset = queryset.filter(date_added__gte=from_date)
        elif to_date:
            queryset = queryset.filter(date_added__lte=to_date)
        else:
            queryset = queryset

        self.stdout.write(
            self.style.SUCCESS('{} registered users deleted'.format(queryset.count()))
        )
        queryset.delete()
