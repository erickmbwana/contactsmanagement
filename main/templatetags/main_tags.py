from django.template import Library


register = Library()


@register.filter
def messagetags(tags):
    types = {
        'success': 'positive',
        'error': 'negative',
        'warning': 'warning',
        'info': 'info'
    }
    for tag in tags.split(' '):
        return types.get(tag, 'info')
