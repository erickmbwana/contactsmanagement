import os
from django.core.exceptions import ValidationError


def validate_file_extension(value):
    _, ext = os.path.splitext(value.name)
    if not ext.lower() == '.csv':
        raise ValidationError("Only csv files are allowed")


def validate_csv_file_size(value):
    limit = 0.25 * 1024 * 1024
    if value.size > limit:
        raise ValidationError(
            '''File too large. Size should not exceed 250kB.
            The app uses a free database which has restrictions on
            the amount of data saved.'''
        )
