from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.shortcuts import reverse

from main.models import Contact
from main.tests.factories import ContactFactory

data = {
    "name": "Rebecca Johnson",
    "email": "rj@gmail.com",
    "phone_number": "+254750136799",
    "age": 34,
}


class ContactViewsTestcase(TestCase):

    def test_post(self):
        url = reverse('contact')

        res = self.client.post(url, data=data)

        self.assertRedirects(res, reverse('contacts'))
        self.assertEqual(Contact.objects.count(), 1)
        self.assertEqual(Contact.objects.first().name, 'Rebecca Johnson')

    def test_update(self):
        # Create
        ContactFactory()
        url = reverse('contact_update', kwargs={'pk': 1})

        self.assertEqual(Contact.objects.first().name, 'John Charo')

        res = self.client.post(url, data=data)

        self.assertRedirects(res, reverse('contacts'))
        self.assertEqual(Contact.objects.first().name, 'Rebecca Johnson')

    def test_delete(self):
        # Create
        ContactFactory()
        url = reverse('contact_delete', kwargs={'pk': 1})

        self.assertEqual(Contact.objects.count(), 1)

        res = self.client.delete(url)

        self.assertRedirects(res, reverse('contacts'))
        self.assertEqual(Contact.objects.count(), 0)


class ContactImportViewTestCase(TestCase):

    def test_post_with_valid_csv_file(self):
        csv_file = open('main/tests/valid_data.csv', 'rb')
        data = {'csv_file': SimpleUploadedFile(csv_file.name, csv_file.read())}

        self.assertEqual(Contact.objects.count(), 0)

        res = self.client.post(reverse('contacts_upload'), data=data)

        self.assertRedirects(res, reverse('contacts'))
        self.assertEqual(Contact.objects.count(), 7)


class ContactDownloadViewTestCase(TestCase):

    def setUp(self):
        data = [
            {"name": "Rebecca Johnson", "email": "reb@gmail.com", "phone_number": "+254750136722", "age": 34},
            {"name": "John Johnson", "email": "jj@gmail.com", "phone_number": "+254758736799", "age": 22},
            {"name": "Shoemaker Crankshaft", "email": "crank@gmail.com", "phone_number": "+254750136799", "age": 45}
        ]
        contacts = [Contact(**kwargs) for kwargs in data]
        Contact.objects.bulk_create(contacts)

    def test_get(self):
        res = self.client.get(reverse('contacts_download'))

        self.assertEqual(res.status_code, 200)
        self.assertTemplateNotUsed(res)
