from collections import OrderedDict

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from main.forms import ContactForm, ContactImportForm, ContactSearchForm
from main.models import Contact


data = {
    "name": "Rebecca Johnson",
    "email": "rj@gmail.com",
    "phone_number": "+254750136799",
    "age": 34,
}


class ContactFormTestCase(TestCase):

    def test_invalid_data(self):
        form = ContactForm(data={})
        self.assertFalse(form.is_valid())

    def test_valid_data(self):
        form = ContactForm(data=data)

        assert form.is_valid()

        form.save()

        assert Contact.objects.count() == 1


class ContactImportFormTestCase(TestCase):

    def test_valid_csv_file(self):
        csv_file = open('main/tests/valid_data.csv', 'rb')
        data = {'csv_file': SimpleUploadedFile(csv_file.name, csv_file.read())}

        form = ContactImportForm(files=data)

        assert form.is_valid()

    def test_invalid_csv_file(self):
        csv_file = open('main/tests/invalid_data.csv', 'rb')
        data = {'csv_file': SimpleUploadedFile(csv_file.name, csv_file.read())}

        form = ContactImportForm(files=data)

        assert form.is_valid() is False


class ContactSearchFormTestCase(TestCase):

    def test_get_filters(self):
        data = OrderedDict([
            ("name", "Rebecca Johnson"),
            ("email", "rj@gmail.com",),
            ("phone_number", "+254750136799")
        ])

        expected = OrderedDict([
            ("name__icontains", "Rebecca Johnson"),
            ("email__exact", "rj@gmail.com",),
            ("phone_number__exact", "+254750136799")
        ])

        form = ContactSearchForm()
        filters = form.get_filters(data=data)

        assert filters == expected
