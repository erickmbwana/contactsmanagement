import json

from django.utils import timezone

from rest_framework import status
from rest_framework.test import APITestCase

from main.models import Contact
from main.tests.factories import ContactFactory

data = {
    "name": "Rebecca Johnson",
    "email": "aaronmatthews@gmail.com",
    "phone_number": "+254750136799",
    "age": 34,
}


class ContactViewSetTestCase(APITestCase):

    def test_post(self):
        res = self.client.post('/api/v1/users/', data=data)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Contact.objects.count(), 1)

    def test_delete(self):
        # Add
        ContactFactory()
        self.assertEqual(Contact.objects.count(), 1)

        # Do a DELETE request
        res = self.client.delete('/api/v1/users/1/')

        self.assertEqual(Contact.objects.count(), 0)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def test_get(self):
        # bulk Create
        today_str = str(timezone.now().date())
        data = [
            {"name": "Rebecca Johnson", "email": "reb@gmail.com", "phone_number": "+254750136722", "age": 34},
            {"name": "John Johnson", "email": "jj@gmail.com", "phone_number": "+254758736799", "age": 22},
            {"name": "Shoemaker Crankshaft", "email": "crank@gmail.com", "phone_number": "+254750136799", "age": 45}
        ]
        contacts = [Contact(**kwargs) for kwargs in data]
        Contact.objects.bulk_create(contacts)

        expected = [
            {
                "id": 2,
                "name": "John Johnson",
                "email": "jj@gmail.com",
                "phone_number": "+254758736799",
                "age": 22,
                "date_added": today_str
            },
            {
                "id": 1,
                "name": "Rebecca Johnson",
                "email": "reb@gmail.com",
                "phone_number": "+254750136722",
                "age": 34,
                "date_added": today_str
            },
            {
                "id": 3,
                "name": "Shoemaker Crankshaft",
                "email": "crank@gmail.com",
                "phone_number": "+254750136799",
                "age": 45,
                "date_added": today_str
            },
        ]
        res = self.client.get('/api/v1/users/')

        actual = json.dumps(res.data['results'], sort_keys=True)
        self.assertJSONEqual(actual, expected)
