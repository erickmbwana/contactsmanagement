from django.db import IntegrityError

from django.test import TestCase
from main.models import Contact

import pytest

from main.tests.factories import ContactFactory

pytestmark = pytest.mark.django_db


class TestSubject(TestCase):

    def test_str(self):
        contact = ContactFactory.build()

        assert str(contact) == 'John Charo'

    def test_create(self):
        Contact.objects.create(name="Ann Munga", age=24, phone_number='+247770000')

        assert Contact.objects.first().name == 'Ann Munga'
        assert Contact.objects.count() == 1

    def test_update(self):
        ContactFactory()

        assert Contact.objects.first().name == 'John Charo'

        Contact.objects.filter(pk=1).update(name='Ann Muye')

        assert Contact.objects.first().name == 'Ann Muye'

    def test_unique_together(self):
        # Same data cannot be added twice
        ContactFactory()
        with self.assertRaises(IntegrityError):
            ContactFactory()
