from factory.django import DjangoModelFactory


class ContactFactory(DjangoModelFactory):

    class Meta:
        model = 'main.Contact'

    name = 'John Charo'
    age = 24
    phone_number = '+25473445654'
    email = 'jcharo@gmail.com'
