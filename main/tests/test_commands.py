import datetime

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase

from main.models import Contact
from main.management.commands.deleterecords import Command as DeleteRecordsCommand


class ImportRecordsCommandTestCase(TestCase):
    '''Tests for the importrecords management command'''

    def test_positional_args_required(self):
        # Test that the command cannot be called without the number_of_stock_records
        msg = 'Error: the following arguments are required: csv_file_path'
        with self.assertRaisesMessage(CommandError, msg):
            call_command('importrecords')

    def test_non_existing_file_path(self):
        msg = 'No such file: me.csv. Check file the path correctly.'
        with self.assertRaisesMessage(CommandError, msg):
            call_command('importrecords', 'me.csv')

    def test_csv_file_with_valid_data(self):
        '''Check that records are created when csv file contains valid data'''

        self.assertEqual(Contact.objects.count(), 0)

        call_command('importrecords', 'main/tests/valid_data.csv')

        self.assertEqual(Contact.objects.count(), 7)

    def test_csv_file_with_invalid_data(self):
        msg = 'Invalid data format in CSV file.'
        with self.assertRaisesMessage(CommandError, msg):
            call_command('importrecords', 'main/tests/invalid_data.csv')

        self.assertEqual(Contact.objects.count(), 0)


class DeleteRecordsCommandTestCase(TestCase):
    '''Tests for the deleterecords management command'''

    def setUp(self):
        data = [
            {"name": "Rebecca Johnson", "email": "reb@gmail.com", "phone_number": "+254750136722", "age": 34},
            {"name": "John Johnson", "email": "jj@gmail.com", "phone_number": "+254758736799", "age": 22},
            {"name": "Shoemaker Crankshaft", "email": "crank@gmail.com", "phone_number": "+254750136799", "age": 45}
        ]
        contacts = [Contact(**kwargs) for kwargs in data]
        Contact.objects.bulk_create(contacts)

    def test_convert_to_date_valid_date_string(self):
        day = datetime.datetime.now().date()
        datestring = day.strftime('%Y/%m/%d')

        cmd = DeleteRecordsCommand()

        self.assertEqual(cmd.convert_to_date(datestring), day)

    def test_convert_to_date_invalid_date_string(self):
        cmd = DeleteRecordsCommand()
        msg = 'Invalid date string: 2012-10-10. Use format: YYYY/MM/DD'

        with self.assertRaisesMessage(CommandError, msg):
            cmd.convert_to_date('2012-10-10')

    def test_records_deleted_properly_if_within_range(self):
        past = datetime.datetime.now() - datetime.timedelta(days=3)
        future = datetime.datetime.now() + datetime.timedelta(days=3)
        from_date = past.strftime('%Y/%m/%d')
        to_date = future.strftime('%Y/%m/%d')

        self.assertEqual(Contact.objects.count(), 3)

        call_command('deleterecords', from_date=from_date, to_date=to_date)

        self.assertEqual(Contact.objects.count(), 0)

    def test_records_deleted_properly_if_no_args(self):
        self.assertEqual(Contact.objects.count(), 3)

        call_command('deleterecords')

        self.assertEqual(Contact.objects.count(), 0)

    def test_records_not_deleted_if_out_of_range(self):
        day1 = datetime.datetime.now() + datetime.timedelta(days=3)
        from_date = day1.strftime('%Y/%m/%d')
        day2 = datetime.datetime.now() - datetime.timedelta(days=3)
        to_date = day2.strftime('%Y/%m/%d')

        self.assertEqual(Contact.objects.count(), 3)

        call_command('deleterecords', from_date=from_date)
        call_command('deleterecords', to_date=to_date)

        self.assertEqual(Contact.objects.count(), 3)
