from django.db import models

from phonenumber_field.modelfields import PhoneNumberField


class Contact(models.Model):
    '''Store a person contact info'''

    name = models.CharField(max_length=255)
    email = models.EmailField()
    phone_number = PhoneNumberField()
    age = models.PositiveSmallIntegerField()
    date_added = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ('name', )
        unique_together = ('name', 'email', 'phone_number', 'age')

    def __str__(self):
        return self.name
